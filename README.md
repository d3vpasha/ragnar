# Ragnar

In order to deploy the application, you have two ways :
- launch the `ragnar.sh` script
- follow manually one of the following methods

## Deploy your application (Docker container) on your environment and expose it on HTTP port

1. Clone this repo

`git clone https://gitlab.com/d3vpasha/ragnar`

2. Enter the repo directory

`cd ragnar`

3. Build the docker image

`docker image build -t docker-image-datadome .`

4. Run the docker container

`docker run -p 5003:5000 -d docker-image-datadome`

5. Open your browser and enter the following :

`http://localhost:5003/`

## Deploy your application on a hosted service (AWS) using Terraform and/or Ansible

- We assume that you already have an AWS account and Ansible is installed on your working environment.
- This deployment was tested using Ansible v2.9.10.
- The Docker image generated in the previous step was pushed to `docker.io` for the purpose of this part. The image is available here : `docker.io/d3vpasha/datadome`

1. Configure your AWS account

`aws configure`

This will ask you for your AWS Access Key ID & AWS Secret Access Key. You can get them from My Security Credentials menu once you are connected.

2. Clone this repo

`git clone https://gitlab.com/d3vpasha/ragnar`

3. Go to the `ansible` directory within the repo

`cd ragnar/ansible`

4. Deploy your app to AWS

`ansible-playbook -i hosts deploy.yml`

5. When the last task will finish, you will have the link to enter on your browser to access the application. Below, an example :

```
TASK [The link to enter on your browser] **************************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": "ec2-15-236-43-135.eu-west-3.compute.amazonaws.com:5000"
}
```
