#!/bin/bash

AWS_CREDENTIALS_FILE=~/.aws/credentials
GIT_REPO_NAME=ragnar

answer=3
while ! [ "$answer" -eq 1 -o "$answer" -eq 2 ]
do
  read -p "`echo -e 'Hello, in which platform you would like to deploy your application ?\n 1) local \n 2) aws\n > '`" answer 
done

case $answer in
  1 )
  echo "You chose to deploy your application locally."
  ;;
  2 )
  echo "You chose to deploy your application on aws"
  if [ ! -f "$AWS_CREDENTIALS_FILE" ]; then
    echo "You didn't configure your aws account yet. Let's do it !"
    read -s -p "Please enter your AWS Access Key ID : " aws_access_key_id  
    echo ''
    read -s -p "Please enter your AWS Secret Access Key : " aws_secret_access_key
    echo ''
    read -p "Please enter your region : " region
    aws configure set aws_access_key_id $aws_access_key_id
    aws configure set aws_secret_access_key $aws_secret_access_key
    aws configure set region $region
  else
    echo "Your aws account is configured."
  fi
  ;;
esac

if [ ! -d "$GIT_REPO_NAME" ]; then
  echo "Let's clone the git repo."
  git clone https://gitlab.com/d3vpasha/ragnar > /dev/null 2>&1
else
  echo "The git repo already exists."
fi

case $answer in
  1 )
  cd $GIT_REPO_NAME
  docker image build -t docker-image-datadome . > /dev/null 2>&1
  docker run -p 5003:5000 -d docker-image-datadome > /dev/null 2>&1
  echo "Please open the following link in your browser : http://localhost:5003/"
  ;;
  2 )
  cd $GIT_REPO_NAME/ansible
  echo "Let's deploy your application!"
  ansible-playbook -i hosts deploy.yml > /dev/null 2>&1
  sleep 1m
  cd ../..
  link=$(cat link)
  echo "Please open the following link in your browser : $link"
  ;;
esac

